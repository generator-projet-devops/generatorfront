import React from "react";
import RowAndColumnSpacing from "../components/Grid";
import Navbar from "../components/Navbar";
const Animals = () => {
  return (
    <>
      <Navbar />
      <RowAndColumnSpacing />
    </>
  );
};

export default Animals;
