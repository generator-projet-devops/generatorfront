import React, { useState } from "react";
import { useForm, Controller } from "react-hook-form";
import {
  Button,
  MenuItem,
  Select,
  TextField,
  FormControl,
  InputLabel,
  FormHelperText,
  CircularProgress,
  List,
} from "@mui/material";
import { Link } from "react-router-dom";


const FORM_STEPS = {
  INIT: "init",
  LOADING: "loading",
  VALIDATION: "validation",
  COMPLETE: "complete",
  ERROR: "error",
};

const AnimalForm = () => {
  const {
    handleSubmit,
    control,
    formState: { errors },
    reset,
  } = useForm();
  const [currentStep, setCurrentStep] = useState(FORM_STEPS.INIT);
  const onSubmit = async (formData) => {
    setCurrentStep(FORM_STEPS.LOADING);
    const apiUrlQuota = "bookQuota/quota";
    const apiUrlDescription = "/story/api/generate";
    const apiUrlAnimal = "image/api/generate";
    const apiUrlApi = "/book/character";
    const jwtToken = localStorage.getItem("token");

    const responseQuota = await fetch(apiUrlQuota, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${jwtToken}`,
      },
    });
    const quotaData = await responseQuota.json();
    if (quotaData.number <= 0) {
      setCurrentStep(FORM_STEPS.ERROR);
    } else {
      let topic = `Description of an animal named ${formData.name}, classified as a ${formData.type} with a ${formData.size} size`;
      if (formData.specialFeature) {
        topic += `, notable for its ${formData.specialFeature}`;
      }
      topic += ` and adorned in a ${formData.color} hue.`;
      const promptForImage = `Please provide an image of an animal named ${formData.name} of type ${formData.type} with a size of ${formData.size}, featuring ${formData.specialFeature} and colored ${formData.color}.`;
      let selectedImageUrl = " ";
      try {
        const responseAnimal = await fetch(apiUrlAnimal, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({ animal: promptForImage }),
        });
        const responseDataAnimal = await responseAnimal.json();
        if (
          Array.isArray(responseDataAnimal.result) &&
          responseDataAnimal.result.length > 0
        ) {
          const randomIndex = Math.floor(
            Math.random() * responseDataAnimal.result.length
          );
          const selectedAnimal = responseDataAnimal.result[randomIndex];
          selectedImageUrl = selectedAnimal.url;
        }
        const responseDescription = await fetch(apiUrlDescription, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({ topic, language: "en" }),
        });
        const responseDataImage = await responseDescription.json();
        await fetch(apiUrlApi, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            name: formData.name,
            description: responseDataImage.result,
            url: selectedImageUrl,
          }),
        });
      } catch (error) {
        console.error("Error fetching data:", error);
      }
      reset();
      setCurrentStep(FORM_STEPS.INIT);
    }
  };

  const renderContent = () => {
    switch (currentStep) {
      case FORM_STEPS.INIT:
        return (
          <div style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center",
            height: "70vh",
          }}>
            <form
              onSubmit={handleSubmit(onSubmit)}
              style={{
                maxWidth: "400px",
                margin: "auto",
                marginTop: "20px",
                padding: "20px",
                border: "1px solid #ccc",
                borderRadius: "8px",
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              <FormControl fullWidth error={!!errors.name}>
                <InputLabel htmlFor="name"> Name</InputLabel>
                <Controller
                  name="name"
                  control={control}
                  defaultValue=""
                  rules={{ required: "The name is required" }}
                  render={({ field }) => <TextField {...field} id="name" />}
                />
                <FormHelperText>{errors.name?.message}</FormHelperText>
              </FormControl>

              <FormControl fullWidth error={!!errors.type}>
                <InputLabel htmlFor="type">Type </InputLabel>
                <Controller
                  name="type"
                  control={control}
                  defaultValue=""
                  rules={{ required: "Type is required" }}
                  render={({ field }) => (
                    <Select {...field} id="type">
                      <MenuItem value="mammal">Mammal</MenuItem>
                      <MenuItem value="reptile">Reptile</MenuItem>
                      <MenuItem value="bird">Bird</MenuItem>
                      <MenuItem value="fish">Fish</MenuItem>
                      <MenuItem value="amphibian">Amphibian</MenuItem>
                      <MenuItem value="insect">Insect</MenuItem>
                    </Select>
                  )}
                />
                <FormHelperText>{errors.type?.message}</FormHelperText>
              </FormControl>

              <FormControl fullWidth error={!!errors.size}>
                <InputLabel htmlFor="size">Size</InputLabel>
                <Controller
                  name="size"
                  control={control}
                  defaultValue=""
                  rules={{ required: "Ce champ est requis" }}
                  render={({ field }) => (
                    <Select {...field} id="size">
                      <MenuItem value="small">Small</MenuItem>
                      <MenuItem value="medium">Medium</MenuItem>
                      <MenuItem value="large">Large</MenuItem>
                    </Select>
                  )}
                />
                <FormHelperText>{errors.size?.message}</FormHelperText>
              </FormControl>

              <FormControl fullWidth error={!!errors.color}>
                <InputLabel htmlFor="color">Color</InputLabel>
                <Controller
                  name="color"
                  control={control}
                  defaultValue=""
                  render={({ field }) => (
                    <TextField
                      {...field}
                      id="color"
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                  )}
                />
                <FormHelperText>{errors.color?.message}</FormHelperText>
              </FormControl>

              <FormControl fullWidth>
                <InputLabel htmlFor="specialFeature">Special Feature</InputLabel>
                <Controller
                  name="specialFeature"
                  control={control}
                  defaultValue=""
                  render={({ field }) => (
                    <TextField
                      {...field}
                      id="specialFeature"
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                  )}
                />
              </FormControl>

              <Button
                type="submit"
                variant="contained"
                color="primary"
                style={{
                  backgroundColor: "#4CAF50",
                  color: "#fff",
                  padding: "10px",
                  borderRadius: "4px",
                  marginTop: "15px",
                  width: "100%",
                }}
              >
                Generate
              </Button>
            </form>
            <Link to="/animal"
              className="text-white bg-gradient-to-r from-green-400 via-green-500 to-green-600 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-green-300 dark:focus:ring-green-800  font-medium rounded-lg text-sm px-5 py-2.5 text-center me-2 mb-2">
              Go To my list
            </Link>
          </div>

        );


      case FORM_STEPS.LOADING:
        return <CircularProgress />;
      case FORM_STEPS.COMPLETE:
        return <CircularProgress />;
      case FORM_STEPS.ERROR:
        return <p>Quota exceeded. Cannot proceed </p>;
      default:
        return null;
    }
  };

  return <div>{renderContent()}</div>;
};

export default AnimalForm;
