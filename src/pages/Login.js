import React, { useState } from "react";
import { useForm } from "react-hook-form";
import Navbar from "../components/Navbar";
import { Link, useNavigate } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";
import showToast from "../utils/ShowToast";

const Login = () => {
  const navigate = useNavigate();
  const goToApp = () => {
    navigate("/app");
    reset();
  };
  const {
    register,
    handleSubmit,
    setError,
    formState: { errors },
    reset,
  } = useForm();
  const [showPassword, setShowPassword] = useState(false);

  const handleTogglePassword = () => {
    setShowPassword(!showPassword);
  };
  const onSubmit = async (formData) => {
    const responseLogin = await fetch("bookAuth/auth/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(formData),
    });

    const result = await responseLogin.json();
    switch (result.statusCode) {
      case 401:
        setError("password", {
          type: "server",
        });
        setError("email", {
          type: "server",
        });
        if (result.message.includes("Validated")) {
          showToast(result.message, "warning");
        } else {
          showToast(
            "Incorrect email or password. Please try again.",
            "warning"
          );
        }

        break;
      case 200:
        await fetch("bookQuota/quota", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${result.access_token}`,
          },
        });
        localStorage.setItem("token", result.access_token);
        showToast("Redirection en cours", "success", () => goToApp());
        break;
      case 500:
        showToast(result.message, "error");
        break;
      default:
        break;
    }
  };

  return (
    <div>
      <Navbar />
      <div
        className="bg-grey-lighter min-h-screen flex flex-col"
        style={{
          backgroundImage:
            "url('https://e0.pxfuel.com/wallpapers/3/702/desktop-wallpaper-tropical-forest-view-daz-3d-gallery-3d-models-and-3d-software-jungle-rain.jpg')",
          backgroundSize: "auto",
        }}
      >
        <form
          autoComplete="on"
          onSubmit={handleSubmit(onSubmit)}
          className={
            "container max-w-sm mx-auto flex-1 flex flex-col items-center justify-center px-2"
          }
        >
          <div className="bg-white/50 px-6 py-8 rounded shadow-md text-black w-full">
            <h1 className={"mb-8 text-3xl text-center"}>Login</h1>
            <input
              type="email"
              className={`block border border-grey-light w-full p-3 rounded mb-4 ${
                errors.email ? "border-red-600" : ""
              }`}
              name="email"
              placeholder="Email"
              {...register("email", {
                required: "This field is required",
                pattern: {
                  value: /^\S+@\S+$/i,
                  message: "Invalid email address",
                },
              })}
            />
            {errors.email && (
              <p className={"text-red-600"}>{errors.email.message}</p>
            )}
            <div className="relative">
              <input
                type={showPassword ? "text" : "password"}
                className={`block border border-grey-light w-full p-3 rounded mb-4 text-neutral-950 ${
                  errors.password ? "border-red-600" : ""
                }`}
                name="password"
                placeholder="Password"
                {...register("password", {
                  required: "The password is required",
                  minLength: {
                    value: 8,
                    message: "Password must be at least 8 characters long",
                  },
                })}
              />
              <button
                type="button"
                onClick={handleTogglePassword}
                className="absolute top-1/2 transform -translate-y-1/2 right-2 px-3 py-1 cursor-pointer bg-white"
              >
                <FontAwesomeIcon icon={showPassword ? faEyeSlash : faEye} />
              </button>
            </div>
            {errors.password && (
              <p className={"text-red-600"}>{errors.password.message}</p>
            )}
            <button
              type="submit"
              className={
                "w-full text-center py-3 rounded bg-green-600 hover:bg-green-200 focus:outline-none my-1"
              }
            >
              Login
            </button>
            <div className="text-grey-dark mt-6">
              You don't have an account?{" "}
              <Link
                to="/register"
                className="no-underline border-b border-blue text-blue"
              >
                Register
              </Link>
            </div>
            <div className="text-grey-dark mt-6">
              You want to validate your account ?{" "}
              <Link
                to="/valid"
                className="no-underline border-b border-blue text-blue"
              >
                Validate
              </Link>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Login;
