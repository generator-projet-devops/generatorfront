
# Étape 1 : Utiliser une image Node.js pour construire l'application
FROM node

# Définir le répertoire de travail dans le conteneur
WORKDIR /app

# Copier le fichier package.json et package-lock.json (si disponible)
COPY package*.json ./

# Installer les dépendances
RUN npm install

# Copier les fichiers du projet dans le conteneur
COPY . .

# Construire l'application React
CMD npm run start
