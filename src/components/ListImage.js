import React, { useEffect, useState } from "react";
import ImageList from "@mui/material/ImageList";
import ImageListItem from "@mui/material/ImageListItem";
import Typography from "@mui/material/Typography";
import ImageListItemBar from "@mui/material/ImageListItemBar";
import IconButton from "@mui/material/IconButton";
import InfoIcon from "@mui/icons-material/Info";
import DeleteIcon from "@mui/icons-material/Delete";
import Modal from "@mui/material/Modal";
import Button from "@mui/material/Button";

const fetchCharacters = async (setCharacters) => {
  const apiUrlApi = "/book/character";
  try {
    const response = await fetch(apiUrlApi, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    });
    const data = await response.json();
    setCharacters(data);
  } catch (error) {
    console.error("Erreur lors de la récupération des personnages :", error);
  }
};

const ModalContent = ({ selectedCharacter, handleCloseModal }) => {
  return (
    <div style={{ textAlign: "center", padding: "20px", background: "#fff", maxWidth: "800px" }}>
      {selectedCharacter && (
        <>
          <img
            src={selectedCharacter.url}
            alt={selectedCharacter.name}
            style={{
              maxWidth: "100%",
              maxHeight: "200px",
              marginBottom: "15px",
            }}
          />
          <h2 id="modal-modal-title">{selectedCharacter.name}</h2>
          <p id="modal-modal-description">{selectedCharacter.description}</p>
        </>
      )}
      <Button onClick={handleCloseModal}>Fermer</Button>
    </div>
  );
};

const CharacterGallery = () => {
  const [characters, setCharacters] = useState([]);
  const [selectedCharacter, setSelectedCharacter] = useState(null);
  const [modalOpen, setModalOpen] = useState(false);

  useEffect(() => {
    fetchCharacters(setCharacters);
  }, []);

  const handleOpenModal = (character) => {
    setSelectedCharacter(character);
    setModalOpen(true);
  };

  const handleCloseModal = () => {
    setModalOpen(false);
  };

  return (
    <div style={{ textAlign: "center" }}>
      <Typography variant="h4" sx={{ marginBottom: 2 }}>
        Mes personnages
      </Typography>

      {characters.length > 0 ? (
        <>
          <ImageList sx={{ width: 500, height: 450 }}>
            <ImageListItem key="Subheader" cols={2}></ImageListItem>
            {characters.map((item, key) => (
              <ImageListItem key={key}>
                <img
                  srcSet={`${item.url}`}
                  src={`${item.url}`}
                  alt={item.name}
                  loading="lazy"
                />
                <ImageListItemBar
                  title={item.name}
                  subtitle={item.description}
                  actionIcon={
                    <>
                      <IconButton
                        sx={{ color: "rgba(255, 255, 255, 0.54)" }}
                        aria-label={`info about ${item.title}`}
                        onClick={() => handleOpenModal(item)}
                      >
                        <InfoIcon />
                      </IconButton>
                      <IconButton
                        sx={{ color: "rgba(255, 255, 255, 0.54)" }}
                        onClick={async () => {
                          try {
                            await fetch(apiUrlApi + "/" + item._id, {
                              method: "DELETE",
                              headers: {
                                "Content-Type": "application/json",
                              },
                            });
                            // Optionally update state or trigger a refetch
                          } catch (error) {
                            console.error("Erreur lors de la suppression du personnage :", error);
                          }
                        }}
                      >
                        <DeleteIcon />
                      </IconButton>
                    </>
                  }
                />
              </ImageListItem>
            ))}
          </ImageList>

          <Modal
            open={modalOpen}
            onClose={handleCloseModal}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
            style={{ display: "flex", alignItems: "center", justifyContent: "center" }}
          >
            <ModalContent selectedCharacter={selectedCharacter} handleCloseModal={handleCloseModal} />
          </Modal>
        </>
      ) : (
        <p>Pas d'image </p>
      )}
    </div>
  );
};

export default CharacterGallery;
