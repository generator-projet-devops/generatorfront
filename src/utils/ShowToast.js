import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const showToast = (text, type = "info", onClose) => {
  const toastOptions = {
    position: "top-right",
    autoClose: 3000,
    hideProgressBar: true,
    closeOnClick: true,
    pauseOnHover: false,
    draggable: false,
    progress: undefined,
  };

  let toastFunction;
  switch (type) {
    case "success":
      toastFunction = toast.success;
      break;
    case "error":
      toastFunction = toast.error;
      break;
    case "warning":
      toastFunction = toast.warn;
      break;
    case "info":
      toastFunction = toast.info;
      break;
    default:
      break;
  }

  if (toastFunction) {
    toastFunction(text, toastOptions);
    toast.onChange((payload) => {
      switch (payload.status) {
        case "added":
          break;
        case "updated":
          break;
        case "removed":
          if (onClose) {
            onClose();
          }
          break;
        default:
          break;
      }
    });
  }
};

export default showToast;
