import React from "react";
import AnimalForm from "../components/AnimalForm";
import Navbar from "../components/Navbar";
import { Link } from "react-router-dom";
const Generator = () => {
  // Vérifier si le token est présent dans le stockage local
  const isTokenPresent = !!localStorage.getItem("token");

  // Vérifier si l'utilisateur est authentifié avant d'afficher AnimalForm et RowAndColumnSpacing
  if (!isTokenPresent) {
    return (
      <>
        <p>Please log in to access this page.</p>
        <Link
          to="/Login"
          className="text-white bg-gradient-to-r from-green-400 via-green-500 to-green-600 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-green-300 dark:focus:ring-green-800  font-medium rounded-lg text-sm px-5 py-2.5 text-center me-2 mb-2"
        >
          Login
        </Link>
      </>
    );
  }

  return (
    <>
      <Navbar />
      <h1 className="text-center text-[40px]">Animal Generator </h1>
      <AnimalForm />
    </>
  );
};

export default Generator;
