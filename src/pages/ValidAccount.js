import React from "react";
import { useForm } from "react-hook-form";
import Navbar from "../components/Navbar";
import { useNavigate } from "react-router-dom";
import showToast from "../utils/ShowToast";

const ValidAccount = () => {
  const navigate = useNavigate();
  const goToLogin = () => {
    navigate("/login");
  };
  const {
    register,
    handleSubmit,
    setError,
    formState: { errors },
  } = useForm();

  const onSubmit = async (formData) => {
    const response = await fetch("bookAuth/user/validate", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(formData),
    });

    const result = await response.json();
    console.log(result);
    if (result === true) {
      showToast("Account validate ", "success", () => goToLogin());
    }
    switch (result.statusCode) {
      case 400:
        setError("email", {
          type: "server",
          message: result.message,
        });
        setError("code", {
          type: "server",
          message: result.message,
        });
        showToast(result.message, "warning");
        break;
      case 500:
        showToast(result.message, "error");
        break;
      default:
        break;
    }
  };

  return (
    <div>
      <Navbar />
      <div
        className="bg-grey-lighter min-h-screen flex flex-col"
        style={{
          backgroundImage:
            "url('https://e0.pxfuel.com/wallpapers/3/702/desktop-wallpaper-tropical-forest-view-daz-3d-gallery-3d-models-and-3d-software-jungle-rain.jpg')",
          backgroundSize: "auto",
        }}
      >
        <form
          autoComplete="on"
          onSubmit={handleSubmit(onSubmit)}
          className="container max-w-sm mx-auto flex-1 flex flex-col items-center justify-center px-2"
        >
          <div className="bg-white/50 px-6 py-8 rounded shadow-md text-black w-full">
            <h1 className="mb-8 text-3xl text-center">Validate your account</h1>
            <input
              type="email"
              className={`block border border-grey-light w-full p-3 rounded mb-4 ${
                errors.email ? "border-red-600" : ""
              }`}
              name="email"
              placeholder="Email"
              {...register("email", {
                required: "Your email is required",
                pattern: {
                  value: /^\S+@\S+$/i,
                  message: "Invalid email address",
                },
              })}
            />
            {errors.email && (
              <p className="text-red-600">{errors.email.message}</p>
            )}
            <input
              type="number" // Assuming the code is a numeric value
              className={`block border border-grey-light w-full p-3 rounded mb-4 ${
                errors.code ? "border-red-600" : ""
              }`}
              name="code"
              placeholder="Code"
              {...register("code", {
                required: "The code is required",
              })}
            />
            {errors.code && (
              <p className="text-red-600">{errors.code.message}</p>
            )}
            <button
              type="submit"
              className="w-full text-center py-3 rounded bg-green-600 hover:bg-green-200 focus:outline-none my-1"
            >
              Confirm
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default ValidAccount;
