import React from "react";
import { Link } from "react-router-dom";
import Navbar from "../components/Navbar";
const Home = () => {
  return (
    <div
      className="leading-normal tracking-normal text-gray-900"
      style={{ fontFamily: "'Source Sans Pro', sans-serif" }}
    >
      <div
        className="h-screen pb-14 bg-right bg-cover"
        style={{ backgroundImage: "url('bg.svg')" }}
      >
        <Navbar />
        <div className="container pt-24 md:pt-48 px-6 mx-auto flex flex-wrap flex-col md:flex-row items-center">
          <div className="flex flex-col w-full xl:w-2/5 justify-center lg:items-start overflow-y-hidden">
            <h1 className="my-4 text-3xl md:text-5xl text-purple-800 font-bold leading-tight text-center md:text-left slide-in-bottom-h1">
              Discover a World of Wonders with Our Generated Animals!
            </h1>
            <p className="leading-normal text-base md:text-2xl mb-8 text-center md:text-left slide-in-bottom-subtitle">
              Unleash your imagination and explore a collection of unique,
              fantastical creatures.
            </p>

            <p className="text-blue-400 font-bold pb-8 lg:pb-6 text-center md:text-left fade-in">
              Try our app:
            </p>
            <div className="flex w-full justify-center md:justify-start pb-24 lg:pb-0 ">
              <Link
                to="/login"
                className="text-gray-900 bg-gradient-to-r from-lime-200 via-lime-400 to-lime-500 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-lime-300 dark:focus:ring-lime-800  font-medium rounded-lg text-sm px-5 py-2.5 text-center me-2 mb-2"
              >
                Login
              </Link>
              <Link
                to="/register"
                className="text-white bg-gradient-to-r from-green-400 via-green-500 to-green-600 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-green-300 dark:focus:ring-green-800  font-medium rounded-lg text-sm px-5 py-2.5 text-center me-2 mb-2"
              >
                Register
              </Link>
            </div>
          </div>

          <div className="w-full xl:w-3/5 py-6 overflow-y-hidden">
            <img
              className="w-5/6 mx-auto lg:mr-0 slide-in-bottom"
              src="/devices.svg"
              alt=""
            />
          </div>

          <div className="w-full pt-16 pb-6 text-sm text-center md:text-left fade-in">
            <p
              className="text-gray-500 no-underline hover:no-underline"
              href="#"
            >
              &copy; App 2023. All Rights Reserved.
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;
