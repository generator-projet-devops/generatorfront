import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Login from './pages/Login';
import Register from './pages/Register';
import Home from './pages/Home';
import Generator from './pages/Generator';
import NotFound from './pages/NotFound';
import ValidAccount from './pages/ValidAccount';
import Animals from './pages/Animals';

const AppRoutes = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/login" element={<Login />} />
        <Route path="/register" element={<Register />} />
        <Route path="/valid" element={<ValidAccount />} />
        <Route path="/app" element={<Generator />} />
        <Route path="/animal" element={<Animals />} />
        <Route path="*" element={<NotFound />} />
      </Routes>
    </BrowserRouter>
  );
};

export default AppRoutes;
