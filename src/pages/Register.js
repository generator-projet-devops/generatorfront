import React, { useState } from "react";
import { useForm } from "react-hook-form";
import Navbar from "../components/Navbar";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";
import showToast from "../utils/ShowToast";
const Register = () => {
  const {
    register,
    handleSubmit,
    setError,
    formState: { errors },
    reset,
  } = useForm();
  const [showPassword, setShowPassword] = useState(false);
  const handleTogglePassword = () => {
    setShowPassword(!showPassword);
  };
  const onSubmit = async (formData) => {
    const responseAnimal = await fetch("bookAuth/auth/register", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(formData),
    });

    const result = await responseAnimal.json();
    switch (result.statusCode) {
      case 400:
        if (result.message.includes("email")) {
          setError("email", {
            type: "server",
            message: result.message,
          });
        }
        if (result.message.includes("username")) {
          setError("username", {
            type: "server",
            message: result.message,
          });
        }
        showToast(result.message, "warning");
        break;
      case 201:
        showToast("Registration successful", "success");
        showToast("Show your email for verify your account!", "success");
        reset();
        break;
      case 500:
        showToast(result.message, "error");
        break;
      default:
        break;
    }
  };

  return (
    <div>
      <Navbar />
      <div
        className="bg-grey-lighter min-h-screen flex flex-col"
        style={{
          backgroundImage:
            "url('https://e0.pxfuel.com/wallpapers/3/702/desktop-wallpaper-tropical-forest-view-daz-3d-gallery-3d-models-and-3d-software-jungle-rain.jpg')",
          backgroundSize: "auto",
        }}
      >
        <form
          autoComplete="on"
          onSubmit={handleSubmit(onSubmit)}
          className={
            "container max-w-sm mx-auto flex-1 flex flex-col items-center justify-center px-2"
          }
        >
          <div className="bg-white/50 px-6 py-8 rounded shadow-md text-black w-full">
            <h1 className={"mb-8 text-3xl text-center"}>Register </h1>
            <input
              type="text"
              {...register("username", {
                required: "The username is required",
              })}
              className={`block border border-grey-light w-full p-3 rounded mb-4 ${
                errors.username ? "border-red-600" : ""
              }`}
              placeholder="Username"
            />
            {errors.username && (
              <p className="text-red-600">{errors.username.message}</p>
            )}
            <input
              type="email"
              className={`block border border-grey-light w-full p-3 rounded mb-4 ${
                errors.email ? "border-red-600" : ""
              }`}
              name="email"
              placeholder="Email"
              {...register("email", {
                required: "This field is required",
                pattern: {
                  value: /^\S+@\S+$/i,
                  message: "Invalid email address",
                },
              })}
            />
            {errors.email && (
              <p className={"text-red-600"}>{errors.email.message}</p>
            )}
            <div className="relative">
              <input
                type={showPassword ? "text" : "password"}
                className={`block border border-grey-light w-full p-3 rounded mb-4 text-neutral-950 ${
                  errors.password ? "border-red-600" : ""
                }`}
                name="password"
                placeholder="Password"
                {...register("password", {
                  required: "The password is required",
                  minLength: {
                    value: 8,
                    message: "Password must be at least 8 characters long",
                  },
                })}
              />
              <button
                type="button"
                onClick={handleTogglePassword}
                className="absolute top-1/2 transform -translate-y-1/2 right-2 px-3 py-1 cursor-pointer bg-white"
              >
                <FontAwesomeIcon icon={showPassword ? faEyeSlash : faEye} />
              </button>
            </div>
            {errors.password && (
              <p className={"text-red-600"}>{errors.password.message}</p>
            )}

            <button
              type="submit"
              className={
                "w-full text-center py-3 rounded bg-green-600 hover:bg-green-200 focus:outline-none my-1"
              }
            >
              Create Account
            </button>
            <div className={"text-grey-dark mt-6"}>
              Already have an account?
              <Link
                to={"/login"}
                className={"no-underline border-b border-blue text-blue"}
              >
                Log in
              </Link>
              .
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Register;
