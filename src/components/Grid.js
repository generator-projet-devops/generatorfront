import React from "react";
import { styled } from "@mui/material/styles";
import Paper from "@mui/material/Paper";
import Box from "@mui/material/Box";
import ListImage from "./ListImage";

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(2),
  textAlign: "center",
  color: theme.palette.text.secondary,
}));

const YourComponent = () => {
  return (
    <Box sx={{ width: "100%" }}>
      <Item>
        <ListImage />
      </Item>
    </Box>
  );
};

export default YourComponent;
